<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/kusamkapoke',function() {
    return 'Masuk Cok';
});
Route::get('/kusam',function(){
    return view('kusam');
});
Route::get('/', function () {
    return view('welcome');
});

Route::get('/halo/{nama}', function($nama) {
    return 'Halo '.$nama;
});

Route::get('/halo/{nama?}', function($nama='koko') {
    return 'Halo '.$nama;
});