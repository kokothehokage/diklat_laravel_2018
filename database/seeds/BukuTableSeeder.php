<?php

use Illuminate\Database\Seeder;

class BukuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('buku')->insert([
            [
                'author' => 'Pak Eko',
                'title' => 'Laravel Howto',
                'year' => 2000,
                'genre_id' => 1
            ],
            [
                'author' => 'Pak Oke',
                'title' => 'Docker Howto',
                'year' => 1999,
                'genre_id' => 2 
            ],
            [
                'author' => 'Pak Yes',
                'title' => 'Sucess Howto',
                'year' => 1996,
                'genre_id' => 3 
            ],
            [
                'author' => 'Pak No',
                'title' => 'Happy Howto',
                'year' => 1982,
                'genre_id' => 4 
            ]
        ]);
        
    }
}
