<?php

use Illuminate\Database\Seeder;

class DeviceElektronikSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('device_elektroniks')->insert([
            [
                'type_id' => '1',
                'brand' => 'Lenovo',
                'series' => 'Ideapad 500S',
                'year' => '2018',
              
            ],
            [
                'type_id' => '2',
                'brand' => 'Samsung',
                'series' => 'Galaxy Tab',
                'year' => '2019',
                
            ],
            [
                'type_id' => '3',
                'brand' => 'Mikrotik',
                'series' => 'RB850',
                'year' => '2016',
                
            ],
            [
                'type_id' => '4',
                'brand' => 'Cisco',
                'series' => 'Catalyst 2960',
                'year' => '2017',
                
            ],
            [
                'type_id' => '5',
                'brand' => 'Schneider',
                'series' => 'MiCOM C264',
                'year' => '2017',
                
            ]
        ]);
    }
}
