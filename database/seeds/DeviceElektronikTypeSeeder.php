<?php

use Illuminate\Database\Seeder;

class DeviceElektronikTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('device_elektronik_types')->insert([
            [
                'type' => 'Notebook',
              
            ],
            [
                'type' => 'Tablet',
                
            ],
            [
                'type' => 'Router',
                
            ],
            [
                'type' => 'Switch',
                
            ],
            [
                'type' => 'RTU',
                
            ]
        ]);
    }
}
